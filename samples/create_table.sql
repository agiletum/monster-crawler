CREATE TABLE public.data
(
    jobid bigint NOT NULL,
    companyname character varying(100) COLLATE pg_catalog."default",
    companyheader character varying(200) COLLATE pg_catalog."default",
    companyindustry character varying(100) COLLATE pg_catalog."default",
    companysize character varying(200) COLLATE pg_catalog."default",
    companyhq character varying(100) COLLATE pg_catalog."default",
    companydesc text COLLATE pg_catalog."default",
    companysumm text COLLATE pg_catalog."default",
    salaryrange character varying(100) COLLATE pg_catalog."default",
    salaryestimate boolean,
    joblocationcountry character varying(50) COLLATE pg_catalog."default",
    joblocationregion character varying(50) COLLATE pg_catalog."default",
    joblocationcity character varying(50) COLLATE pg_catalog."default",
    jobdescription text COLLATE pg_catalog."default",
    datasource character varying(50) COLLATE pg_catalog."default" NOT NULL,
    jobcategory character varying(80) COLLATE pg_catalog."default",
    positiontype text COLLATE pg_catalog."default",
    jobindustry text COLLATE pg_catalog."default",
    CONSTRAINT data_pkey PRIMARY KEY (jobid)
)
