#!/usr/bin/env node

'use strict';
const fetch = require('node-fetch');

const { Pool } = require('pg')
const client = new Pool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: 'agile',
    port: 5432
});

const min = Number.parseInt(process.env.MIN_CRAWL) || 0;
const max = Number.parseInt(process.env.MAX_CRAWL) || min + 50;

// https://www.monster.com/jobs/search/pagination/?q=agile&where=United-States&intcid=skr_navigation_nhpso_searchMain&isDynamicPage=true&isMKPagination=true&page=6&total=60


var id2crawl = [];

var page = min;

const iterate = () => {
    if(page==max){
        return;
    }
    if (id2crawl.length == 0) {
        let search = `https://www.monster.com/jobs/search/pagination/?q=agile&where=United-States&intcid=skr_navigation_nhpso_searchMain&isDynamicPage=true&isMKPagination=true&page=${page}&total=60`;
        page+=1;
        fetch(search).then(response=>response.json()).then(x=>{
            return x;
        }).then(arr=> id2crawl = arr.map(x=>x.MusangKingId).filter(x=>x));
        console.log("ids have been updated.");
    }
    console.log("ids in cache: "+id2crawl.length);
    let jobid = id2crawl.pop();
    const url = "https://job-openings.monster.com/v2/job/pure-json-view?jobid=" + jobid;
    var errors = 0;
    fetch(url).then(response => response.json()).then(({ jobId, companyInfo, salary, jobLocationCountry, jobLocationRegion, jobLocationCity, jobDescription, jobCategory, jobIndustry, postedDate }) => {
        var v = {
            "jobid": jobId,
            "datasource": "monster.com",
            "companyname": companyInfo.name,
            "companyheader": companyInfo.companyHeader,
            "companyindustry": companyInfo.industryName,
            "companysize": companyInfo.companySizeName,
            "companyhq": companyInfo.hqLocation,
            "companydesc": companyInfo.description,
            "companysumm": null,
            "salaryrange": salary ? salary.rangeText : null,
            "salaryestimate": salary ? salary.estimation : null,
            "joblocationcountry": jobLocationCountry,
            "joblocationregion": jobLocationRegion,
            "joblocationcity": jobLocationCity,
            "jobdescription": jobDescription,
            "jobcategory": jobCategory,
            "jobindustry": jobIndustry,
            "posteddate": postedDate
        };
        console.log(`ID: ${jobId}\nJob Offer: ${v.companyheader}\nCompany Industry: ${companyInfo.industryName}\npostedDate: ${v.posteddate}`);
        var keys = Object.keys(v);
        const query = "INSERT INTO data (" + keys.join(" ,") + ") VALUES(" + keys.map((_, i) => "$" + (i + 1)).join(" ,") + ")";
        client.query(query, keys.map(key => v[key]), (err) => console.log(err ? err.stack : "saved to db"));
        errors = 0;
    }).catch(ex => console.error(ex) && errors++);
    if (errors < 5) setTimeout(() => iterate(jobid + 1), 2000);
}
iterate();
